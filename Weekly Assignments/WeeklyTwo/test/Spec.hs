module Main where

import Test.Hspec
import TriTree

main :: IO ()
main = hspec $ do

  describe "identical" $ do
    it "Empty and Empty are identical" $
      (identical (Empty :: (TriTree Int)) (Empty :: (TriTree Int))) `shouldBe` True

    it "insertList [1,2..100] and insertList [1,2..100] are identical" $
      (identical (insertList [1,2..100] Empty) (insertList [1,2..100] Empty)) `shouldBe` True

    it "insertList [1,2..100] and insertList [1,2..101] are not identical" $
      (identical (insertList [1,2..100] Empty) (insertList [1,2..101] Empty)) `shouldBe` False

  describe "search" $ do
    it "found 50 in [1,2..100]" $
      (search 50 (insertList [1,2..100] Empty)) `shouldBe` True

    it "did not find 50 in []" $
      (search 50 (Empty)) `shouldBe` False

    it "found \"Am I Present?\" in [A phrase., A second phrase., Am I Present?, A, B]" $
      (search "Am I Present?" (insertList ["A phrase.", " A second phrase.", "Am I Present?", "A", "B"] Empty)) `shouldBe` True

  describe "insert" $ do
    it "inserted 5 into Empty" $
      (identical (insert 5 Empty) (NodeOne 5 Empty Empty Empty)) `shouldBe` True

    it "inserted 5 into (NodeTwo 1 2 Empty Empty Empty)" $
      (identical (insert 5 (NodeTwo 1 2 Empty Empty Empty) ) (NodeTwo 1 2 Empty Empty (NodeOne 5 Empty Empty Empty)) ) `shouldBe` True

    it "inserted (-1) into (NodeOne 2 Empty Empty Empty)" $
      (identical (insert (-1) (NodeOne 2 Empty Empty Empty) ) (NodeTwo (-1) 2 Empty Empty Empty) ) `shouldBe` True

  describe "insertList" $ do
    it "inserted [1,2,3,4,5] into Empty" $
      (identical (insertList [1,2..5] Empty) (NodeTwo 1 2 Empty Empty (NodeTwo 3 4 Empty Empty (NodeOne 5 Empty Empty Empty)))) `shouldBe` True

    it "inserted [] into Empty" $
      (identical (insertList [] Empty) (Empty :: (TriTree Int))) `shouldBe` True
    
    it "inserted [a, b, c, abc, def, a, b, c] into Empty" $
      (identical (insertList ["a", "b", "c", "abc", "def", "a", "b", "c"] Empty) (NodeTwo "a" "b" Empty (NodeTwo "a" "abc" Empty Empty (NodeOne "b" Empty Empty Empty)) (NodeTwo "c" "def" Empty (NodeOne "c" Empty Empty Empty) Empty))) `shouldBe` True
    
  describe "treeMap" $ do
    it "mapped (+1) on NodeTwo 1 2 Empty Empty (NodeTwo 3 4 Empty Empty (NodeOne 5 Empty Empty Empty)))" $
      (identical (treeMap (+1) (insertList [1,2,3,4,5] Empty)) (NodeTwo 2 3 Empty Empty (NodeTwo 4 5 Empty Empty (NodeOne 6 Empty Empty Empty))) ) `shouldBe` True

    it "mapped (*2) on NodeTwo 1 2 Empty Empty (NodeTwo 3 4 Empty Empty (NodeOne 5 Empty Empty Empty)))" $
      (identical (treeMap (*2) (insertList [1,2,3,4,5] Empty)) (NodeTwo 2 4 Empty Empty (NodeTwo 6 8 Empty Empty (NodeOne 10 Empty Empty Empty))) ) `shouldBe` True

    it "mapped (*5) on NodeTwo 2 4 Empty Empty (NodeTwo 6 8 Empty Empty (NodeOne 10 Empty Empty Empty)))" $
      (identical (treeMap (*5) (insertList [2,4..10] Empty)) (NodeTwo 10 20 Empty Empty (NodeTwo 30 40 Empty Empty (NodeOne 50 Empty Empty Empty))) ) `shouldBe` True

  describe "treeFoldPreOrder" $ do
    it "folded insertList [1,2..100] pre ordered" $
      (treeFoldPreOrder (+) 0 (insertList [1,2..100] Empty) ) `shouldBe` 5050

    it "folded insertList [1,2..100] pre ordered" $
      (treeFoldPreOrder (-) 0 (insertList [1,2..100] Empty) ) `shouldBe` (-5050)

    it "folded insertList [a, b, c, d, e, f, g, h, i] pre ordered" $
      (treeFoldPreOrder (++) "" (insertList ["a", "z", "b", "c", "d", "f", "g", "h", "i"] Empty) ) `shouldBe` "azbcdfghi"

  describe "treeFoldInOrder" $ do
    it "folded insertList [1,2..100] in ordered" $
      (treeFoldInOrder (+) 0 (insertList [1,2..100] Empty) ) `shouldBe` 5050

    it "folded insertList [1,2..100] in ordered" $
      (treeFoldInOrder (-) 0 (insertList [1,2..100] Empty) ) `shouldBe` (-5050)

    it "folded insertList [a, b, c, d, e, f, g, h, i] in ordered" $
      (treeFoldInOrder (++) "" (insertList ["a", "z", "b", "c", "d", "f", "g", "h", "i"] Empty) ) `shouldBe` "abcdfghiz"

  describe "treeFoldPostOrder" $ do
    it "folded insertList [1,2..100] post ordered" $
      (treeFoldPostOrder (+) 0 (insertList [1,2..100] Empty) ) `shouldBe` 5050

    it "folded insertList [1,2..100] post ordered" $
      (treeFoldPostOrder (-) 0 (insertList [1,2..100] Empty) ) `shouldBe` (-5050)

    it "folded insertList [a, b, c, d, e, f, g, h, i] post ordered" $
      (treeFoldPostOrder (++) "" (insertList ["a", "z", "b", "c", "d", "f", "g", "h", "i"] Empty) ) `shouldBe` "ighdfbcaz"

  describe "buildTree" $ do
     it "built a tree with the elements [1,2,3,4]" $
       (identical (buildTree [1,2,3,4] Empty) (NodeTwo 3 4 (NodeTwo 1 2 Empty Empty Empty) Empty Empty) ) `shouldBe` True

     it "built a tree with the elements [a,b,c,d]" $
       (identical (buildTree ['a','b','c','d'] Empty) (NodeTwo 'c' 'd' (NodeTwo 'a' 'b' Empty Empty Empty) Empty Empty) ) `shouldBe` True
     
     it "built a tree with the elements []" $
       (identical (buildTree ([] :: [Char]) Empty) (Empty) ) `shouldBe` True


    

    