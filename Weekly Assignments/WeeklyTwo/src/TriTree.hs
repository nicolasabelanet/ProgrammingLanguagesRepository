-- Nicolas Abelanet
-- 10/6/21
-- Programming Languages
-- TriTree.hs

module TriTree where

-- TriTree
--     A TriTree is the root node.
--     A node has 3 posibilites for storing data.
--     1. The node is empty.
--     2. The node contains a single value and left and middle children.
--     3. The node contains two values and left, middle, and right children.
data TriTree a = Empty | 
                 NodeOne a (TriTree a) (TriTree a) (TriTree a) | 
                 NodeTwo a a (TriTree a) (TriTree a) (TriTree a) 
                 deriving (Show)

-- search
--     Searches through a tree to find wether or not 
--     a given value in present in a tree.
--     Takes in a value, a tree, and return a bool.
search :: (Eq a, Ord a) => a -> TriTree a -> Bool
search target Empty = False
search target (NodeOne val left middle Empty) 
                                             | target == val = True
                                             | target < val = search target left
                                             | target > val = search target middle
search target (NodeTwo leftVal rightVal left middle right)
                                                          | (target == leftVal) || (target == rightVal) = True
                                                          | target < leftVal = search target left
                                                          | (target > leftVal) && (target < rightVal) = search target middle
                                                          | target > rightVal = search target right
                                                                    
-- insert
--     Inserts a given value in to a tree in the proper place.
--     Takes in a value, a tree, and returns the new tree.
insert :: (Eq a, Ord a) => a -> TriTree a -> TriTree a
insert newVal Empty = NodeOne newVal Empty Empty Empty
insert newVal (NodeOne val Empty Empty Empty)
                                             | newVal > val = 
                                                    NodeTwo val newVal Empty Empty Empty
                                             | newVal < val = 
                                                    NodeTwo newVal val Empty Empty Empty
                                             | newVal == val = 
                                                    NodeTwo newVal val Empty Empty Empty

insert newVal (NodeOne val left middle Empty)
                                             | newVal == val =
                                                    NodeTwo newVal newVal left middle Empty    
                                             | newVal < val =
                                                    NodeOne val (insert newVal left) middle Empty
                                             | newVal > val =
                                                    NodeOne val left (insert newVal middle) Empty  

insert newVal (NodeTwo leftVal rightVal left middle right)
                                                          | newVal < leftVal = 
                                                                 NodeTwo leftVal rightVal (insert newVal left) middle right
                                                          | (newVal == leftVal) || (newVal == rightVal) = 
                                                                 NodeTwo leftVal rightVal left (insert newVal middle) right
                                                          | (newVal > leftVal) && (newVal < rightVal) = 
                                                                 NodeTwo leftVal rightVal left (insert newVal middle) right
                                                          | newVal > rightVal = 
                                                                 NodeTwo leftVal rightVal left middle (insert newVal right)

-- insertList
--     Inserts a list of values in to a tree all in their proper places.
--     This function uses insert.
--     Takes in a list of values, a tree, and returns the new tree.
insertList :: (Ord a) => [a] -> TriTree a -> TriTree a
insertList [] tree = tree 
insertList (x : xs) tree = insertList xs (insert x tree)

-- identical
--     Identical determines wether or not a tree is exactly the same as another.
--     This includes same number of values, same respective values, and exact
--     same tree structure.
--     Takes in two trees and returns a bool.
identical :: (Eq a) => TriTree a -> TriTree a -> Bool
identical Empty Empty = True
identical (NodeOne _ _ _ _) Empty = False
identical Empty (NodeOne _ _ _ _) = False
identical (NodeTwo _ _ _ _ _) Empty = False
identical Empty (NodeTwo _ _ _ _ _) = False
identical (NodeOne _ _ _ _) (NodeTwo _ _ _ _ _) = False
identical (NodeTwo _ _ _ _ _) (NodeOne _ _ _ _) = False
identical (NodeOne valA leftA middleA Empty) (NodeOne valB leftB middleB Empty) = 
        (valA == valB) && (identical leftA leftB) && (identical middleA middleB)
identical (NodeTwo valLeftA valRightA leftA middleA rightA) (NodeTwo valLeftB valRightB leftB middleB rightB) = 
       (valLeftA == valLeftB) && (valRightA == valRightB) && (identical leftA leftB) 
       && (identical middleA middleB) && (identical rightA rightB)

-- treeMap
--     Takes in a generic function and applies it to each node
--     of a tree. The generic function has the same type
--     as the new tree that will be returned.
--     Takes in a generic function, a tree, and returns a new tree. 
treeMap :: (a -> b) -> TriTree a -> TriTree b
treeMap f Empty = Empty
treeMap f (NodeOne val left middle Empty) = NodeOne (f val) (treeMap f left) (treeMap f middle) Empty
treeMap f (NodeTwo leftVal rightVal left middle right) = NodeTwo (f leftVal) (f rightVal) (treeMap f left) (treeMap f middle) (treeMap f right)

-- treeFoldPreOrder
--     Combines values in a tree acording to some generic function.
--     Follows the preorder tree traversal method. The generic function has
--     the same return type as the type of input tree and output tree.
--     Takes in a generic function, a tree, and returns a new tree.
treeFoldPreOrder :: (a -> a -> a) -> a -> TriTree a -> a
treeFoldPreOrder f acc Empty = acc
treeFoldPreOrder f acc (NodeOne val left middle Empty) = treeFoldPreOrder f (treeFoldPreOrder f (f acc val) left) middle
treeFoldPreOrder f acc (NodeTwo leftVal rightVal left middle right) =  treeFoldPreOrder f (treeFoldPreOrder f (treeFoldPreOrder f (f (f acc leftVal) rightVal) left) middle) right

-- treeFoldInOrder
--     Combines values in a tree acording to some generic function.
--     Follows the inorder tree traversal method. The generic function has
--     the same return type as the type of input tree and output tree.
--     Takes in a generic function, a tree, and returns a new tree.
treeFoldInOrder :: (a -> a -> a) -> a -> TriTree a -> a
treeFoldInOrder f acc Empty = acc
treeFoldInOrder f acc (NodeOne val left middle Empty) = treeFoldInOrder f (f (treeFoldInOrder f acc left) val) middle
treeFoldInOrder f acc (NodeTwo leftVal rightVal left middle right) = treeFoldInOrder f (f (treeFoldInOrder f (f (treeFoldInOrder f acc left) leftVal) middle) (rightVal)) right

-- treeFoldPostOrder
--     Combines values in a tree acording to some generic function.
--     Follows the postorder tree traversal method. The generic function has
--     the same return type as the type of input tree and output tree.
--     Takes in a generic function, a tree, and returns a new tree.
treeFoldPostOrder :: (a -> a -> a) -> a -> TriTree a -> a
treeFoldPostOrder f acc Empty = acc
treeFoldPostOrder f acc (NodeOne val left middle Empty) = f (treeFoldPostOrder f (treeFoldPostOrder f acc left) middle) val
treeFoldPostOrder f acc (NodeTwo leftVal rightVal left middle right) = f (f (treeFoldPostOrder f (treeFoldPostOrder f (treeFoldPostOrder f acc left) middle) right) leftVal) rightVal

-- buildTree
--     Inserts a list of values in to a tree all in their proper places.
--     Unlike insertList this function creates the tree from leaf up.
--     This function uses insert.
--     Takes in a list of values, a tree, and returns the new tree.                                          
buildTree :: (Ord a) => [a] -> TriTree a -> TriTree a
buildTree [] tree = tree 
buildTree (x : xs) tree = insert x (buildTree xs tree)




