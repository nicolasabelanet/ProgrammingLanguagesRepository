-- Nicolas Abelanet
-- 10/25/21
-- Programming Languages
-- Spec.hs

module Main where

import Test.Hspec
import WeeklyThree

main :: IO ()
main = hspec $ do
  describe "firstAnswer" $ do
    it "found the first answer of [1,2,3]" $
      (firstAnswer (\x -> Just x) [1,2,3] ) `shouldBe` Just 1

    it "found the first answer of [1,2,3]" $
      (firstAnswer (\x -> Nothing) [1,2,3] ) `shouldBe` (Nothing :: Maybe Int)

    it "found the first answer of []" $
      (firstAnswer (\x -> Just 1) [] ) `shouldBe` (Nothing :: Maybe Int)
  
  describe "allAnswers" $ do
    it "found all the answers of [1,2,3]" $
      (allAnswers (\x -> Just [x]) [1,2,3] ) `shouldBe` Just [1,2,3]
    
    it "found all the answers of []" $
      (allAnswers (\x -> Just [x]) [] ) `shouldBe` (Nothing :: (Maybe [Int]))

    it "found all the answers of [1,2,3]" $
      (allAnswers (\x -> Just [x]) [] ) `shouldBe` (Nothing :: (Maybe [Int]))

  describe "getStrings" $ do
    it "found all the variables strings of WildcardPat" $
      (getStrings WildcardPat ) `shouldBe` ([] :: [String])

    it "found all the variables strings of VariablePat" $
      (getStrings (VariablePat "A String") ) `shouldBe` ["A String"]

    it "found all the variables strings of UnitPat" $
      (getStrings UnitPat ) `shouldBe` ([] :: [String])

    it "found all the variables strings of ConstantPat" $
      (getStrings (ConstantPat 2) ) `shouldBe` ["2"]

    it "found all the variables strings of ConstructorPat" $
      (getStrings (ConstructorPat ("A String", VariablePat "a")) ) `shouldBe` ["A String", "a"]

    it "found all the variables strings of TuplePat" $
      (getStrings (TuplePat [VariablePat "a", ConstructorPat ("b", VariablePat "c")]) ) `shouldBe` ["a", "b", "c"]

  describe "repeated" $ do
    it "found if there are any repeated strings in [a, b, c, d]" $
      (repeated ["a", "b", "c", "d"] ) `shouldBe` False

    it "found if there are any repeated strings in [a, a, c, d]" $
      (repeated ["a", "a", "c", "d"] ) `shouldBe` True

    it "found if there are any repeated strings in []" $
      (repeated [] ) `shouldBe` False

  describe "checkPat" $ do
    it "all the variables appearing in WildcardPat are distinct" $
      (checkPat WildcardPat ) `shouldBe` True

    it "all the variables appearing in VariablePat are distinct" $
      (checkPat (VariablePat "a") ) `shouldBe` True

    it "all the variables appearing in UnitPat are distinct" $
      (checkPat UnitPat ) `shouldBe` True

    it "all the variables appearing in ConstructorPat are distinct" $
      (checkPat ( ConstructorPat ("a", WildcardPat) ) ) `shouldBe` True

    it "all the variables appearing in ConstructorPat aren't distinct" $
      (checkPat ( ConstructorPat ("a", VariablePat "a") ) ) `shouldBe` False

    it "all the variables appearing in TuplePat are distinct" $
      (checkPat (TuplePat [VariablePat "a", VariablePat "b"]) ) `shouldBe` True

    it "all the variables appearing in TuplePat aren't distinct" $
      (checkPat (TuplePat [VariablePat "a", VariablePat "a"]) ) `shouldBe` False

  describe "match" $ do
    it "matches all patterns and values" $
      ( match ( Tuple[Unit, Constructor ("c1", Unit)], TuplePat[VariablePat "p1", VariablePat "p2"] ) ) `shouldBe` Just [("p1",Unit),("p2",Constructor ("c1",Unit))]

    it "matches all patterns and values" $
      ( match (Unit, TuplePat[VariablePat "p1", VariablePat "p2"] ) ) `shouldBe` (Nothing :: Maybe[(String, Value)] )

    it "matches all patterns and values" $
      ( match (Constant 1, VariablePat "v" ) ) `shouldBe` Just [("v", Constant 1)]

    it "matches all patterns and values" $
      ( match (Tuple [Unit, Constant 2], WildcardPat ) ) `shouldBe` Just []

    it "matches all patterns and values" $
      ( match (Constructor("a", Unit), ConstructorPat ("a", UnitPat) ) ) `shouldBe` Just []

  describe "firstMatch" $ do
    it "matches the value with the first applicable pattern" $
      ( firstMatch ( Constructor ("a", Unit) )  [ConstantPat 1, ConstructorPat("a",UnitPat)] ) `shouldBe` Just []

    it "matches the value with the first applicable pattern" $
      ( firstMatch ( Constructor ("a", Unit) )  [] ) `shouldBe` (Nothing :: Maybe[(String, Value)] )

    it "matches the value with the first applicable pattern" $
      ( firstMatch ( Constructor ("a", Unit) )  [WildcardPat] ) `shouldBe` Just []

    it "matches the value with the first applicable pattern" $
      ( firstMatch ( Constructor ("a", Unit) )  [UnitPat] ) `shouldBe` (Nothing :: Maybe[(String, Value)] )




    
    