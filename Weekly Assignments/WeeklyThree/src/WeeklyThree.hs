-- Nicolas Abelanet
-- 11/6/21
-- Programming Languages
-- WeeklyThree.hs

module WeeklyThree where

import Data.Maybe

data Pattern = WildcardPat | 
               VariablePat (String) | 
               UnitPat | 
               ConstantPat (Int) | 
               ConstructorPat (String, Pattern) | 
               TuplePat ([Pattern])     
               deriving (Eq, Show)

data Value = Constant (Int) | 
             Unit | 
             Constructor (String, Value) | 
             Tuple [Value] 
             deriving (Eq, Show)

-- firstAnswer applies the first element (a function) to elements of the second argument (a list) 
-- in order until the first time it returns Just v for some v and then Just v is the result 
-- of the call to firstAnswer. If the first argument returns Nothing for all the elements in the list, 
-- then firstAnswer should return Nothing. The function that is passed can be thought of as a predicate 
-- It returns Nothing or Just v for particular types of a if it determines those are invalid or valid 
-- answers. Note: The function should produce Nothing if there are not any matches. 
firstAnswer :: (a -> Maybe b) -> [a] -> Maybe b
firstAnswer f [] = Nothing
firstAnswer f (x : xs) = case result of
                            (Just x) -> Just x
                            (_) -> firstAnswer f xs 
                        where 
                            result = f x

-- allAnswers applies the first element (a function) to elements of the second argument (a list). 
-- If it returns Nothing for any element, then the result for allAnswers is Nothing. Otherwise, 
-- calls to the first argument will have produced Just lst1, Just lst2, ..., Just lstn and the 
-- result of allAnswers is Just lst, where lst is [lst1 ++ lst2 ++ ... ++ lstn].
allAnswers :: (a -> Maybe [b]) -> [a] -> Maybe [b]
allAnswers _ [] = Nothing
allAnswers f list = if (containsNothing result)
                        then Nothing
                        else Just (foldl (++) [] (catMaybes result))
                    where 
                        result = map f list
                        containsNothing [] = False
                        containsNothing (x : xs) = if isNothing x
                                                   then True
                                                   else containsNothing xs

-- getStrings  takes a pattern and returns a list of all the strings it uses for variables.
getStrings :: Pattern -> [String]
getStrings x = case x of
                (WildcardPat) -> []
                (VariablePat string) -> [string]
                (UnitPat) -> []
                (ConstantPat num) -> [show num]
                (ConstructorPat (string, pattern) ) -> [string] ++ (getStrings pattern)
                (TuplePat patterns) -> foldl getTuples [] patterns
        where   
            getTuples x y = x ++ (getStrings y)

-- repeated takes a list of strings and determines if it has repeats and returns true
-- if the list does contain repeats and false otherwise.
repeated :: [String] -> Bool
repeated xs = search xs []
        where 
            search [] _ = False
            search (x : xs) searched = if elem x searched
                                       then True
                                       else search xs (x : searched)

-- checkPat takes a Pattern and returns true if and only if all the variables appearing in 
-- the pattern are distinct from each other (i.e., they are different strings) and false 
-- otherwise. The constructor names are not relevant. 
checkPat :: Pattern -> Bool
checkPat pattern = not ( repeated (getStrings pattern) )

-- match takes a (Value, Pattern) and returns a Maybe [(String, Value)] (note the list is inside 
-- the Maybe type, it is not a list of Maybe types). It produces Nothing if the Pattern does not 
-- match and Just lst where lst is the list of bindings if it does. Note that if the Value matches 
-- but the Pattern has no Patterns of the form VariablePat s, then the result is Just []. 
match :: (Value, Pattern) -> Maybe [(String, Value)]
match (value, pattern) = case (value, pattern) of
    (_, WildcardPat) -> Just []
    (_, VariablePat string) -> Just [(string, value)]
    (Unit, UnitPat) -> Just []
    (Constructor (s1, v), ConstructorPat (s2, p) ) -> if (s1 == s2)
                                                         then match (v, p)
                                                         else Nothing
    (Tuple vs, TuplePat ps) -> if (length vs == length ps)
                               then allAnswers (match) (zip vs ps)
                               else Nothing
    (_, _) -> Nothing

-- firstMatch takes a Value and a list of Patterns and returns a Maybe [(String, Value)], in particular,
-- Nothing if no pattern in the list matches, or Just lst, where lst is the list of bindings for the 
-- first pattern in the list that matches.
firstMatch :: Value -> [Pattern] -> Maybe [(String, Value)]
firstMatch value patterns = firstAnswer (match) (assembleTuples value patterns)
                        where 
                            assembleTuples v [] = []
                            assembleTuples v (x : xs) = (value, x) : (assembleTuples v xs)
                            

      

