-- Nicolas Abelanet
-- 11/18/21
-- Programming Languages
-- Spec.hs

module Main where

import Test.Hspec
import TinyEvaluator
import TinyDefinitions

main :: IO ()
main = hspec $ do
  describe "compileAndRun" $ do
    it "correctly evaluates (1 add 1)" $
      (compileAndRun "(1 add 1)" ) `shouldBe` (IntegerType 2)

    it "correctly evaluates (1 multiply 2)" $
      (compileAndRun "(1 multiply 2)") `shouldBe` (IntegerType 2)

    it "correctly evaluates (1 add (second (pair 1 2)))" $
      (compileAndRun "(1 add (second (pair 1 2)))") `shouldBe` (IntegerType 3)

    it "correctly evaluates (1 add (first (pair 1 2)))" $
      (compileAndRun "(1 add (first (pair 1 2)))") `shouldBe` (IntegerType 2)

    it "correctly evaluates (let n be 1 in (1 divide n))" $
      (compileAndRun "(let n be 1 in (1 divide n))") `shouldBe` (IntegerType 1)

    it "correctly evaluates (let n be 1 in (if (1 equals n) then (5 multiply n) else (10 subtract n)))" $
      (compileAndRun "(let n be 1 in (if (1 equals n) then (5 multiply n) else (10 subtract n) ) )") `shouldBe` (IntegerType 5)

    it "correctly evaluates (let n be 1 in (if (true and (1 equals n)) then (true or false) else (10 subtract n)))" $
      (compileAndRun "(let n be 1 in (if (true and (1 equals n)) then (true or false) else (10 subtract n) ) )") `shouldBe` (BoolType True)

    it "correctly evaluates (let f be (lambda n in (if (0 equals n) then 1 else 0)) in (call f with 0))" $
      (compileAndRun "(let f be (lambda n in (if (0 equals n) then 1 else 0)) in (call f with 0))") `shouldBe` (IntegerType 1)
    