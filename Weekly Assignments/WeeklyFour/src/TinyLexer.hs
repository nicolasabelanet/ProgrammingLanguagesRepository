-- Nicolas Abelanet
-- 11/18/21
-- Programming Languages
-- TinyLexer.hs

module TinyLexer where

import Control.Applicative 
import MonadicParserLibrary 

import TinyDefinitions

trueConst :: Parser ParseTree 
trueConst = do symbol "true"
               return (ValueNode (BoolType True))

falseConst :: Parser ParseTree 
falseConst = do symbol "false"
                return (ValueNode (BoolType False))

boolConst :: Parser ParseTree 
boolConst = do trueConst 
             <|> falseConst

andOp :: Parser String
andOp = symbol "and"

orOp :: Parser String
orOp = symbol "or"

notOp :: Parser String
notOp = symbol "not"

boolOp :: Parser String
boolOp = do andOp
           <|> orOp
           <|> notOp 

leftParenthesis :: Parser String
leftParenthesis = do symbol "("

rightParenthesis :: Parser String
rightParenthesis = do symbol ")"

addOp :: Parser String
addOp = do symbol "add"

subtractOp :: Parser String
subtractOp = do symbol "subtract"

multiplyOp :: Parser String
multiplyOp = do symbol "multiply"

divideOp :: Parser String
divideOp = do symbol "divide"

remainderOp :: Parser String
remainderOp = do symbol "remainder"

mathOp :: Parser String
mathOp = do addOp
          <|> subtractOp
          <|> multiplyOp
          <|> divideOp
          <|> remainderOp

integerConst :: Parser ParseTree
integerConst = do num <- integer
                  return (ValueNode (IntegerType num))

variable :: Parser ParseTree
variable = do i <- ident
              return (IdNode i)

equalKeyword :: Parser String
equalKeyword = symbol "equals"

callKeyword :: Parser String
callKeyword = symbol "call"

inKeyword :: Parser String
inKeyword = symbol "in"

lambdaKeyword :: Parser String
lambdaKeyword = symbol "lambda"

letKeyword :: Parser String
letKeyword = symbol "let"

beKeyword :: Parser String
beKeyword = symbol "be"

pairKeyword :: Parser String
pairKeyword = symbol "pair"

withKeyword :: Parser String
withKeyword = symbol "with"

ifKeyword :: Parser String
ifKeyword = symbol "if"

thenKeyword :: Parser String
thenKeyword = symbol "then"

elseKeyword :: Parser String
elseKeyword = symbol "else"

firstKeyword :: Parser String
firstKeyword = symbol "first"

secondKeyword :: Parser String
secondKeyword = symbol "second"
