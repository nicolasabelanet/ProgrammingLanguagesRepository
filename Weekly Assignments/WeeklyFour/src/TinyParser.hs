-- Nicolas Abelanet
-- 11/18/21
-- Programming Languages
-- TinyParser.hs

module TinyParser where

--    A Parser for things
-- Is a function from strings
--     To lists of pairs
--   Of things and strings

import TinyDefinitions
import TinyLexer

import MonadicParserLibrary
import Control.Applicative 

-- parseString
--   Consume a string containing a Tiny Language program 
--   Produce a structure representing the parse tree of the program 
-- parseString :: String -> [(ParseTree,String)]
parseString :: String -> ParseTree
parseString program = let [(tree,remainingChars)] = parse expressionParser program
                          in
                             case remainingChars of 
                                  "" -> tree
                                  _  -> error ("Parse Error: " ++ remainingChars)

-- expressionParser
--   Produce a parser for an expression in the Tiny Language
--     The parser will produce a ParseTree representing the 
--         program 
expressionParser :: Parser ParseTree  
expressionParser = do expr <- exprLevelOne
                      return expr
                    <|>
                   do leftParenthesis
                      pairKeyword
                      exprOne <- expressionParser
                      exprTwo <- expressionParser
                      rightParenthesis
                      return (ValueNode (PairType exprOne exprTwo))
                    <|> 
                   do leftParenthesis
                      letKeyword
                      i <- ident
                      beKeyword
                      expr <- expressionParser 
                      inKeyword
                      body <- expressionParser
                      rightParenthesis
                      return (LetNode i expr body)
                    <|> 
                   do leftParenthesis
                      lambdaKeyword
                      i <- ident
                      inKeyword
                      bodyExpr <- expressionParser
                      rightParenthesis
                      return (LambdaNode i bodyExpr)
                    <|>
                   do leftParenthesis
                      callKeyword
                      i <- ident
                      withKeyword
                      body <- expressionParser
                      rightParenthesis
                      return (CallNode i body)
                    <|>
                   do leftParenthesis
                      ifKeyword
                      boolExpr <- expressionParser
                      thenKeyword
                      thenExpr <- expressionParser
                      elseKeyword 
                      elseExpr <- expressionParser
                      rightParenthesis
                      return (IfNode boolExpr thenExpr elseExpr)
                    <|>
                   do leftParenthesis
                      exprOne <- expressionParser
                      equalKeyword
                      exprTwo <- expressionParser
                      rightParenthesis
                      return (EqualsNode exprOne exprTwo)
                    <|>
                   do leftParenthesis
                      firstKeyword
                      pairExpr <- expressionParser
                      rightParenthesis
                      case pairExpr of
                          (ValueNode (PairType exprOne exprTwo)) -> return (OneOfNode exprOne)
                          _ -> error "Incorrect Usage of First Keyword"
                    <|>
                   do leftParenthesis
                      secondKeyword
                      pairExpr <- expressionParser
                      rightParenthesis
                      case pairExpr of
                          (ValueNode (PairType exprOne exprTwo)) -> return (OneOfNode exprTwo)
                          _ -> error "Incorrect Usage of Second Keyword"
                    <|>     
                   do leftParenthesis
                      expr <- expressionParser
                      rightParenthesis  
                      return expr  

-- Lowest level of precedence of Boolean Expressions 
--    This handles the boolean or operation
exprLevelOne :: Parser ParseTree 
exprLevelOne = do exprOne <- exprLevelTwo
                  do op <- orOp
                     exprTwo <- expressionParser 
                     return (OrNode exprOne exprTwo)
                   <|> do op <- addOp
                          exprTwo <- expressionParser
                          return (AdditionNode exprOne exprTwo)
                   <|> do op <- subtractOp
                          exprTwo <- expressionParser
                          return (SubtractionNode exprOne exprTwo)
                   <|> 
                     return exprOne 

-- Second level of precedence of Boolean Expressions
--    This handles the boolean and operation 
exprLevelTwo :: Parser ParseTree
exprLevelTwo = do exprOne <- exprLevelThree
                  do op <- andOp
                     exprTwo <- expressionParser
                     return (AndNode exprOne exprTwo)
                   <|> do op <- multiplyOp
                          exprTwo <- expressionParser 
                          return (MultiplicationNode exprOne exprTwo)
                   <|> do op <- divideOp
                          exprTwo <- expressionParser
                          return (DivisionNode exprOne exprTwo) 
                   <|>
                     return exprOne

-- Third level of precedence of Boolean Expressions
--     This handles the boolean not operation
exprLevelThree :: Parser ParseTree
exprLevelThree = do op <- notOp
                    expr <- expressionParser
                    return (NotNode expr)
                  <|>
                 do leftParenthesis
                    expr <- expressionParser
                    rightParenthesis
                    return expr
                  <|>
                 do b <- boolConst 
                    return b     
                  <|>
                 do v <- variable 
                    return v
                  <|>
                 do num <- integerConst  
                    return num

