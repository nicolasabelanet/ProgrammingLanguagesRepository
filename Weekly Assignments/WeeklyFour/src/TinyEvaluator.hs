-- Nicolas Abelanet
-- 11/18/21
-- Programming Languages
-- TinyEvaluator.hs

module TinyEvaluator where

import TinyParser
import TinyLexer
import Env

import TinyDefinitions

-- compileAndRun
--   Consumes a String which is the program
--   Produces the result of lexing, parsing, and evaluating the program
compileAndRun :: String -> ValueType
compileAndRun program = evaluate (parseString program) emptyEnv

-- evaluate
--   Consume a Parse Tree
--   Produce the result value of evaluating the given Parse Tree
evaluate :: ParseTree -> EnvType -> ValueType 
evaluate tree env = case tree of
                      (ValueNode (BoolType val)) -> BoolType val
                      (ValueNode (ClosureType val)) -> ClosureType val
                      (ValueNode (IntegerType val)) -> IntegerType val
                      (ValueNode (PairType exprOne exprTwo)) -> PairType exprOne exprTwo
                      (IdNode var) -> (applyEnv var env)
                      (NotNode val) -> let param = (evaluate val env)
                                       in 
                                            case param of 
                                               (BoolType True) -> (BoolType False)
                                               (BoolType False) -> (BoolType True)
                      (AndNode valOne valTwo) -> let paramOne = (evaluate valOne env)
                                                     paramTwo = (evaluate valTwo env)
                                                   in
                                                     case paramOne of 
                                                        (BoolType True) -> 
                                                           case paramTwo of
                                                             (BoolType True) -> (BoolType True)
                                                             (BoolType False) -> (BoolType False)
                                                        (BoolType False) -> (BoolType False)
                      (OrNode valOne valTwo) -> let paramOne = (evaluate valOne env)
                                                    paramTwo = (evaluate valTwo env)
                                                  in
                                                    case paramOne of
                                                       (BoolType True) -> (BoolType True)
                                                       (BoolType False) ->
                                                           case paramTwo of
                                                                (BoolType True) -> (BoolType True)
                                                                (BoolType False) -> (BoolType False)
                      (AdditionNode valOne valTwo) -> let paramOne = (evaluate valOne env)
                                                          paramTwo = (evaluate valTwo env)
                                                        in
                                                          case (paramOne, paramTwo) of
                                                               (IntegerType intOne, IntegerType intTwo) -> IntegerType (intOne + intTwo)
                      (SubtractionNode valOne valTwo) -> let paramOne = (evaluate valOne env)
                                                             paramTwo = (evaluate valTwo env)
                                                          in
                                                            case (paramOne, paramTwo) of
                                                                (IntegerType intOne, IntegerType intTwo) -> IntegerType (intOne - intTwo)
                      (MultiplicationNode valOne valTwo) -> let paramOne = (evaluate valOne env)
                                                                paramTwo = (evaluate valTwo env)
                                                              in
                                                                case (paramOne, paramTwo) of
                                                                    (IntegerType intOne, IntegerType intTwo) -> IntegerType (intOne * intTwo)
                      (DivisionNode valOne valTwo) -> let paramOne = (evaluate valOne env)
                                                          paramTwo = (evaluate valTwo env)
                                                        in
                                                          case (paramOne, paramTwo) of
                                                               (IntegerType intOne, IntegerType intTwo) -> IntegerType (intOne `divide` intTwo)
                      (RemainderNode valOne valTwo) -> let paramOne = (evaluate valOne env)
                                                           paramTwo = (evaluate valTwo env)
                                                        in
                                                          case (paramOne, paramTwo) of
                                                               (IntegerType intOne, IntegerType intTwo) -> IntegerType (intOne `mod` intTwo)
                      (LetNode id val body) -> let valResult = (evaluate val env )
                                                           in 
                                                             (evaluate body 
                                                               (extendEnv (id, valResult) env))
                      (IfNode valOne valTwo valThree) -> let paramOne = (evaluate valOne env)
                                                             in 
                                                               case (paramOne) of
                                                               (BoolType val) -> if (val == True)
                                                                                    then (evaluate valTwo env)
                                                                                    else (evaluate valThree env)
                      (EqualsNode valOne valTwo) -> let paramOne = (evaluate valOne env)
                                                        paramTwo = (evaluate valTwo env)
                                                             in 
                                                               case (paramOne, paramTwo) of
                                                                  (IntegerType intOne, IntegerType intTwo) -> BoolType (intOne == intTwo)
                                                                  (BoolType boolOne, BoolType boolTwo) -> BoolType (boolOne == boolTwo)
                                                                  _ -> BoolType False
                      (OneOfNode val) -> evaluate val env                                                              
                      (LambdaNode id body) -> ClosureType (Closure id body env)
                      (CallNode functionName expr) -> 
                           let result = applyEnv functionName env
                              in 
                                case result of
                                    ClosureType (Closure paramName functionBody functionEnv) ->
                                         (evaluate functionBody 
                                              (extendEnv (paramName, (evaluate expr env)) env))
                                    _ -> error "Illegal function call"

-- divide performs integer division.
-- Takes in two integers and returns a third.
divide :: Integer -> Integer -> Integer
divide a b = div a b 0
          where div a b c | (b < 0) = 0
                          | (a < b) = c
                          | (a >= b) = div (a-b) b (c+1)
                      

                   
