-- Nicolas Abelanet
-- 11/18/21
-- Programming Languages
-- TinyDefinitions.hs

module TinyDefinitions where

  data ParseTree = AndNode ParseTree ParseTree |
                   OrNode ParseTree ParseTree  |
                   NotNode ParseTree           |
                   ValueNode ValueType         |
                   IdNode String               |
                   LetNode String ParseTree ParseTree |
                   LambdaNode String ParseTree |
                   CallNode String ParseTree |
                   AdditionNode ParseTree ParseTree |
                   SubtractionNode ParseTree ParseTree |
                   MultiplicationNode ParseTree ParseTree |
                   DivisionNode ParseTree ParseTree |
                   RemainderNode ParseTree ParseTree |
                   IfNode ParseTree ParseTree ParseTree |
                   EqualsNode ParseTree ParseTree |
                   OneOfNode ParseTree |
                   EmptyNode
                    deriving (Show)
  
  -- closure structure
  data ClosureStructure = Closure String ParseTree EnvType 
                          deriving (Show)

  data ValueType = BoolType Bool |
                   IntegerType Integer |
                   PairType ParseTree ParseTree | 
                   ClosureType ClosureStructure
                    deriving (Show)

  instance Eq ValueType where
    (==) (IntegerType valOne) (IntegerType valTwo) = valOne == valTwo
    (==) (BoolType boolOne) (BoolType boolTwo) = boolOne == boolTwo

  type EnvType = [(String,ValueType)]
