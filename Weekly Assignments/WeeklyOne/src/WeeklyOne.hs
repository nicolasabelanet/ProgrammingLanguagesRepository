module WeeklyOne where

-- removeChar consumes a single character and a string. 
-- Removes all instances of a character from a string
-- and returns said string.

removeChar :: Char -> [Char] -> [Char]
removeChar _ [] = []
removeChar a (b : bs) = if (b == a)
                        then removeChar a bs
                        else b : (removeChar a bs)

-- removeChar consumes a list of characters and a string. 
-- Removes all instances of every character in the list from a string
-- and returns said string.

removeChars :: [Char] -> [Char] -> [Char]
removeChars [] [] = []
removeChars (a : as) b = if (null as)
                         then removeChar a b
                         else removeChars as (removeChar a b)

-- removeWhitespace consumes a string. 
-- Produces a new string with all spaces, tabs, new line characters, 
-- and carriage returns removed. This function should compose instances 
-- of removeChar.

whiteSpace = [' ', '\n', '\r', '\t']

removeWhitespace :: [Char] -> [Char]
removeWhitespace a = removeChars whiteSpace a


-- removePunctuation consumes a string. Produces a new string with all 
-- commas, periods, parentheses, square brackets, and curly brackets removed. 

punctuation = [',', '.', '(', ')', '[', ']', '{', '}']

removePunctuation :: [Char] -> [Char]
removePunctuation a = removeChars punctuation a

-- charsToAscii consumes a string. Produces a new list containing the 
-- ASCII values of the characters in the given string. 

charsToAscii :: [Char] -> [Int]
charsToAscii [] = []
charsToAscii (a : as) = (fromEnum a) : (charsToAscii as)

-- asciiToChars consumes a list of integers (which are assumed to be 
-- valid ASCII values). Produces a new list of characters created 
-- from the ASCII values.

asciiToChars :: [Int] -> [Char]
asciiToChars [] = []
asciiToChars (a : as) = (toEnum a) : (asciiToChars as)

-- shiftInts consumes an integer (the shift value) and a 
-- list of integers (which are assumed to be valid ASCII values). 
-- Produces a new list of integers where each value in the 
-- given list has been increased by the shift value (modulo 128
-- which is the maximum ASCII value). For example, shiftInts 1 
-- [2, 4, 6] should produce [3, 5, 7].

shiftInts :: Int -> [Int] -> [Int]
shiftInts _ [] = []
shiftInts a (b : bs) = (mod (a + b) 128) : (shiftInts a bs)

-- shiftMessage consumes an integer (the shift value) 
-- and a string (the message). Produces a new string 
-- which is the encrypted message where each character 
-- has been shifted by the shift value in the ASCII encoding. 
-- This can also be used to decrypt a message using a 
-- negative integer as the shift value.

shiftMessage :: Int -> [Char] -> [Char]
shiftMessage a b = asciiToChars ( shiftInts a (charsToAscii b) )