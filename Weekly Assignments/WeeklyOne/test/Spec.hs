module Main where

import Test.Hspec
import WeeklyOne

main :: IO ()
main = hspec $ do
  describe "removeChar" $ do
    it "removes a from \"aaStringaFilleadwithaaaaaas\"" $
      (removeChar 'a' "aaStringaFilleadwithaaaaaas") `shouldBe` "StringFilledwiths"

    it "removes a from \"\"" $
      (removeChar 'a' "") `shouldBe` ""

    it "removes w from \"wwwwwwwwwwwwwwwwwwwwwww\"" $
      (removeChar 'w' "wwwwwwwwwwwwwwwwwwwwwww") `shouldBe` ""

  describe "removeWhitespace" $ do
    it "removes whitespace from \"a string with lots of \t \n \r whitespace\"" $
      (removeWhitespace "a string with lots of \t \n \r whitespace") `shouldBe` "astringwithlotsofwhitespace"

    it "removes whitespace from \"\"" $
      (removeWhitespace "") `shouldBe` ""

    it "removes whitespace from \"\t\t\t\t\t\t\t\t\t\t\t\t\t         \r\r\r\r\r\r\r\n\r\n\r      \t\t\t\r\t\"" $
      (removeWhitespace "\t\t\t\t\t\t\t\t\t\t\t\t\t         \r\r\r\r\r\r\r\n\r\n\r      \t\t\t\r\t") `shouldBe` ""

  describe "removePunctuation" $ do
    it "removes punctuation from \"a.[]{}.,,,,.[]{()} string with lots of punctuation.\"" $
      (removePunctuation "a.[]{}.,,,,.[]{()} string with lots of punctuation.") `shouldBe` "a string with lots of punctuation"

    it "removes punctuation from \"\"" $
      (removePunctuation "") `shouldBe` ""

    it "removes punctuation from \"...{,}..]..}..,,[],,,,],,,,{{{{){){{){]{,[[[[,[{{{{((,(((())()(){}{{({}\"" $
      (removePunctuation "...{,}..]..}..,,[],,,,],,,,{{{{){){{){]{,[[[[,[{{{{((,(((())()(){}{{({}") `shouldBe` ""


  describe "shiftMessage" $ do
    it "correctly encoded and decoded \"The Message\"" $
      (shiftMessage (-4) (shiftMessage (4) "The Message")) `shouldBe` "The Message"

    it "correctly encoded and decoded \"\"" $
      (shiftMessage (-4) (shiftMessage (4) "")) `shouldBe` ""

    it "correctly encoded and decoded \"This message is complex and contains (.,+-).\"" $
      (shiftMessage (-10) (shiftMessage (10) "This message is complex and contains (.,+-).")) `shouldBe` "This message is complex and contains (.,+-)."