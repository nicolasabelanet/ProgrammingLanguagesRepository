-- Nicolas Abelanet
-- 10/4/21
-- Programming Languages
-- Spec.hs

module Main where

import Test.Hspec
import DailyFive

main :: IO ()
main = hspec $ do
  describe "multPairs" $ do
    it "multiplies [(1,2), (3,4), (4,5)]" $
      (multPairs [(1,2), (3,4), (4,5)]) `shouldBe` [2, 12, 20]

    it "multiplies [(1,8), (76,4), (69,69)]" $
      (multPairs [(1,8), (76,4), (69,69)]) `shouldBe` [8,304,4761]

    it "multiplies [(3,8), (0,4), (88,4)]" $
      (multPairs [(3,8), (0,4), (88,4)]) `shouldBe` [24,0,352]

  describe "squareList" $ do
    it "squares the list [1,3,2]" $
      (squareList [1,3,2]) `shouldBe` [(1,1), (3, 9), (2, 4)]

    it "squares the list [0,1,30]" $
      (squareList [0,1,30]) `shouldBe` [(0,0), (1, 1), (30, 900)]

    it "squares the list [7,6,5]" $
      (squareList [7,6,5]) `shouldBe` [(7,49), (6, 36), (5, 25)]

  describe "findLowercase" $ do
    it "finds the lower cases of the list [Hi, my, name, is, Joe]" $
      (findLowercase ["Hi", "my", "name", "is", "Joe"]) `shouldBe` [False, True, True, True, False]

    it "finds the lower cases of the list [i, am, Hungry, please, give, me, Food]" $
      (findLowercase ["i", "am", "Hungry", "please", "give", "me", "Food"]) `shouldBe` [True, True, False, True, True, True, False]

    it "finds the lower cases of the list [SIMPLY, ALL, UPPERCASE]" $
      (findLowercase ["SIMPLY", "ALL", "UPPERCASE"]) `shouldBe` [False, False, False]

    
   
   

 