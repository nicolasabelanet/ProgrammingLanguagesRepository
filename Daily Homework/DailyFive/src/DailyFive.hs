-- Nicolas Abelanet
-- 10/4/21
-- Programming Languages
-- DailyFive.hs

module DailyFive where
import Data.Char
 
-- multPair multiplies a pair of integers and returns the result.
multPair :: (Integer, Integer) -> Integer
multPair (x1, x2) = x1 * x2 

--multPairs consumes a list of pairs of integers and produces 
-- a list of the products of each pair.
multPairs :: [(Integer, Integer)] -> [Integer]
multPairs x = map multPair x

-- square takes in an integer and returns a pair of integers with
-- the first element being just the integer and second 
-- the square of the integer.
square :: Integer -> (Integer, Integer)
square x = (x, x*x)

-- squareList consumes a list of Integers as input and produces 
-- a new list of pairs of Integers. The resulting pairs 
-- should be the original Integer and the square of that integer.
squareList :: [Integer] -> [(Integer, Integer)]
squareList x = map square x

-- isFirst lower returns wether or not the first
-- character in a string is lowercase.
isFirstLower :: [Char] -> Bool
isFirstLower (x : xs) = isLower x

-- findLowercase that consumes a list of String 
-- and produces a list of Bool. An entry in 
-- the produced list should be True if the corresponding 
-- String starts with a lower case character and False otherwise. 
-- You may assume that each String has at least one character.
findLowercase :: [[Char]] -> [Bool]
findLowercase x = map isFirstLower x

