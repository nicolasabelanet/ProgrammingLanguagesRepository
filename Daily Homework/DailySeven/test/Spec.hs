-- Nicolas Abelanet
-- 10/14/21
-- Programming Languages
-- Spec.hs

module Main where

import Test.Hspec
import DailySeven

main :: IO ()
main = hspec $ do
  describe "createOneList" $ do
    it "creates ones list from the list [ [1,2], [3], [ ], [4, 5] ]" $
      (createOneList [ [1,2], [3], [ ], [4, 5] ]) `shouldBe` [1,2,3,4,5]

    it "creates ones list from the list [ [a], [a,b], [a,b,c] ]" $
      (createOneList [ ['a'], ['a','b'], ['a','b','c'] ]) `shouldBe` ['a','a','b','a','b','c']

    it "creates ones list from the list []" $
      (createOneList ([] :: [[Int]]) ) `shouldBe` ([] :: [Int])

  describe "findLargest" $ do
    it "finds the largest in [1,2,3,4,5]" $
      (findLargest [1,2,3,4,5]) `shouldBe` 5

    it "finds the largest in [5,4,3,2,1]" $
      (findLargest [5,4,3,2,1]) `shouldBe` 5

    it "finds the largest in []" $
      (findLargest []) `shouldBe` 0
  
  describe "allTrue" $ do
    it "determines wether [True, True, True] is all true" $
      (allTrue [True, True, True]) `shouldBe` True
    
    it "determines wether [False, True, True] is all true" $
      (allTrue [False, True, True]) `shouldBe` False

    it "determines wether [] is all true" $
      (allTrue []) `shouldBe` True