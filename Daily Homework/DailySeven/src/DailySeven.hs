-- Nicolas Abelanet
-- 10/14/21
-- Programming Languages
-- DailySeven.hs

module DailySeven where

-- createOneList consumes a list of lists of some type. 
-- The function produces a single list which 
-- contains all the elements contained in the lists.
createOneList :: [[a]] -> [a]
createOneList list = foldl (++) [] list

-- findLargest consumes a list of positive integer numbers. 
-- The function should produces the largest number contained 
-- in the list.
findLargest :: [Int] -> Int
findLargest list = foldl (\x y -> if (x > y) then x else y) 0 list

-- allTrue consumes a list of boolean values and produces True 
-- if all the elements of the list are True and False 
-- otherwise.
allTrue :: [Bool] -> Bool
allTrue list = foldl (&&) True list