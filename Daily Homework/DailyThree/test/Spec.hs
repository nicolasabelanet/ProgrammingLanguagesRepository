-- Nicolas Abelanet
-- 9/26/21
-- Programming Languages
-- Spec.hs

module Main where

import Test.Hspec
import DailyThree

main :: IO ()
main = hspec $ do
  describe "removeAllExcept" $ do
    it "removes everything except all instances of a from [b, a, c, a]" $
      ( removeAllExcept 'a' ['b', 'a', 'c', 'a']) `shouldBe` "aa"

    it "removes everything except all instances of 1 from [2, 3, 4, 1]" $
      ( removeAllExcept 1 [2, 3, 4, 1]) `shouldBe` [1]

    it "removes everything except all instances of D from [A, B, C]" $
      ( removeAllExcept 'D' ['A', 'B', 'C']) `shouldBe` []

  describe "countOccurrences" $ do
    it "counts all occurencs of a in [a, b, a, c]" $
      ( countOccurrences 'a' ['a', 'b', 'a', 'c']) `shouldBe` 2

    it "counts all occurencs of 1 in [2, 4, 5, 2]" $
      ( countOccurrences 1 [2, 4, 5, 2]) `shouldBe` 0

    it "counts all occurencs of a in []" $
      ( countOccurrences 1 [] ) `shouldBe` 0

  describe "substitute" $ do
    it "substitutes all instances of 3 with 4" $
      (  substitute 3 4 [1, 2, 3, 4]) `shouldBe` [1, 2, 4, 4]

    it "substitutes all instances of a with b" $
      ( substitute 'a' 'b' "abcdaa") `shouldBe` "bbcdbb"

    it "substitutes all instances of c with d" $
      ( substitute 'c' 'd' [] ) `shouldBe` []


 
   