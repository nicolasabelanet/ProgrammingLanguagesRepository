-- Nicolas Abelanet
-- 9/25/21
-- Programming Languages
-- DailyThree.hs

module DailyThree where

-- removeAllExcept will remove everything that is not equal to the given element. 
removeAllExcept :: Eq a => a -> [a] -> [a]
removeAllExcept _ [] = []
removeAllExcept a (b : bs) = if (b == a)
                             then b : (removeAllExcept a bs)
                             else removeAllExcept a bs

-- countOccurences counts how many times a given element occurs in the given list
countOccurrences :: Eq a => a -> [a] -> Int
countOccurrences _ [] = 0
countOccurrences a (b : bs) = if (b == a)
                              then 1 + (countOccurrences a bs)
                              else countOccurrences a bs

-- substitute replaces all occurrences of the first argument with 
-- the second argument in the list and return the new list.
substitute :: Eq a => a -> a -> [a] -> [a]
substitute _ _ [] = []
substitute a b (c : cs) = if (c == a)
                          then b : (substitute a b cs)
                          else c : (substitute a b cs)
 