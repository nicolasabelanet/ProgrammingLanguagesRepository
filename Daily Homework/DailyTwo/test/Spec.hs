module Main where

import Test.Hspec
import DailyTwo

main :: IO ()
main = hspec $ do
  describe "everyThird" $ do
    it "produces everyThird element of [1,2,3,4,5,6]" $
      (everyThird [1,2,3,4,5,6]) `shouldBe` [3,6]

    it "produces everyThird element of [10, 15, 2, 5, -1, 4, 5, -10, -1.5, -2, 5.42]" $
      (everyThird [10, 15, 2, 5, -1, 4, 5,  -10, -1.5, -2, 5.42]) `shouldBe` [2, 4, -1.5]

    it "produces everyThird element of [a, b, c, d, e ,f]" $
      (everyThird ["a", "b", "c", "d", "e", "f"]) `shouldBe` ["c", "f"]
  
    it "produces everyThird element of [1]" $
      (everyThird [1]) `shouldBe` []
  
  describe "tupleDotProduct" $ do
    it "produces dot product of [1, 2] and [2, 3]" $
      (tupleDotProduct [1,2] [2,3]) `shouldBe` 8

    it "produces dot product of [1] and [-5]" $
      (tupleDotProduct [1] [-5]) `shouldBe` -5

    it "produces dot product of [1.5, 2.3] and [1, 2]" $
      (tupleDotProduct [1.5,2.3] [1,2]) `shouldBe` 6.1

    it "produces dot product of [-1.5, 2.3] and [1, -2]" $
      (tupleDotProduct [-1.5,2.3] [1,-2]) `shouldBe` (-6.1)

  describe "appendToEach" $ do
    it "produces the correct result of !!! and [Hello, Goodbye]" $
      (appendToEach "!!!" [ "Hello", "Goodbye" ]) `shouldBe` [ "Hello!!!", "Goodbye!!!" ]

    it "produces the correct result of appended and [1, 2, 3, 4]" $
      (appendToEach "appended" [ "1", "2", "3", "4" ]) `shouldBe` [ "1appended", "2appended", "3appended", "4appended" ]

    it "produces the correct result of [] and [[]]" $
      (appendToEach [] [[]]) `shouldBe` [""]
