module DailyTwo where

-- everyThird takes a list and constructs a new list only consisting 
-- of every third element in it. This means that calling it on [1, 2, 3, 4, 5, 6] 
-- would return [3, 6]. Returns the same type as the input list.

-- There is a slight issue with the robustnes of this function. I can't quite figure
-- out how to ensure a non null list would not cause issues without restricting the type.

-- everyThird :: [x] -> [x]
-- everyThird a = [ a!!x | x <- [2,5..(length a - 1)] ]
everyThird :: [x] -> [x]
everyThird [] = []
everyThird [_] = []
everyThird [_ , _] = []
everyThird (one : two : three : ls) = three : everyThird ls 



-- tupleDotProduct takes two lists of numbers and finds the dot product between them. 
-- It is assumed that both lists will be the same size. Both lists and the result are all
-- of the same type.

-- tupleDotProduct (a : as) (b : bs) = if (null as || null bs) 
                                    -- then a * b
                                    -- else a * b + tupleDotProduct as bs

tupleDotProduct :: (Num x) => [x] -> [x] -> x
tupleDotProduct [] [] = 0
tupleDotProduct (a : as) (b : bs) = a * b + tupleDotProduct as bs

                

-- appendToEach takes a string and a list of strings. The function produces a new list where 
-- the string is appended to each string in the list. For example: appendToEach "!!!" [ "Hello", "Goodbye" ] 
-- should produce [ "Hello!!!", "Goodbye!!!" ]. 

-- appendToEach a (b : bs) = if (null bs)
                        --   then [b ++ a]
                        --   else (b ++ a) : appendToEach a bs
                        
appendToEach :: [Char] -> [[Char]] -> [[Char]]
appendToEach _ [] = []
appendToEach a (b : bs) = (b ++ a) : appendToEach a bs

