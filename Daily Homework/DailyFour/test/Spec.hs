-- Nicolas Abelanet
-- 9/19/21
-- Programming Languages
-- Spec.hs

module Main where

import Test.Hspec
import DailyFour

main :: IO ()
main = hspec $ do
  describe "zip3Lists" $ do
    it "zips [1, 2, 3], ['a', 'b', 'c'] and [4, 5, 6]" $
      (zip3Lists [1, 2, 3] ['a', 'b', 'c'] [4, 5, 6]) `shouldBe` [(1, 'a', 4), (2, 'b', 5), (3, 'c', 6)]

    it "zips [1], [2], and [3]" $
      (zip3Lists [1] [2] [3]) `shouldBe` [(1,2,3)]

    it "zips [First], [Second], [Third]" $
      (zip3Lists ["First"] ["Second"] ["Third"]) `shouldBe` [("First","Second","Third")]

  describe "unzipTriples" $ do
    it "unzips [ (1,2,3), (4, 5, 6), (7, 8, 9) ]" $
      (unzipTriples [ (1,2,3), (4, 5, 6), (7, 8, 9) ]) `shouldBe` ([1,4,7],[2,5,8],[3,6,9])

    it "unzips [(1),(2),(3)]]" $
      (unzipTriples [ (1,2,3)] ) `shouldBe` ([1], [2], [3])

    it "unzips [ (1,2,3), (4, 5, 6), (7, 8, 9), (10,11,12), (13,14,15) ]" $
      (unzipTriples [ (1,2,3), (4, 5, 6), (7, 8, 9), (10,11,12), (13,14,15) ]) `shouldBe` ([1,4,7,10,13],[2,5,8,11,14],[3,6,9,12,15])

  describe "mergeSorted3" $ do
    it "merges and sorts [2, 3, 5], [1, 8], and [-1, 0, 4, 10]" $
      ( mergeSorted3 [2, 3, 5] [1, 8] [-1, 0, 4, 10]) `shouldBe` [-1, 0, 1, 2, 3, 4, 5, 8, 10]

    it "merges and sorts [], [1, 8], and [-1, 0, 4, 10]" $
      ( mergeSorted3 [] [1, 8] [-1, 0, 4, 10]) `shouldBe` [-1, 0, 1, 4, 8, 10]

    it "merges and sorts [1,1,1,1,1], [-10, 1, 8], and [-1, -5, 0, 4, 10]" $
      ( mergeSorted3 [1,1,1,1,1] [-10, 1, 8] [-1, -5, 0, 4, 10]) `shouldBe` [-10,-1,-5,0,1,1,1,1,1,1,4,8,10]

