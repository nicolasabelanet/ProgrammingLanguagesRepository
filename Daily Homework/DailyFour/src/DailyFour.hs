-- Nicolas Abelanet
-- 9/29/21
-- Programming Languages
-- DailyFour.hs

module DailyFour where
 
-- zip3Lists takes three lists as arguments and creates a list of tuples.
zip3Lists :: [a] -> [b] -> [c] -> [(a,b,c)]
zip3Lists [] [] [] = []
zip3Lists (a : as) (b : bs) (c : cs) = (a,b,c) : (zip3Lists as bs cs)

-- unzipTriples takes a list of triples and produces a tuple of three lists.
unzipTriples :: [(a,a,a)] -> ([a], [a], [a])
unzipTriples [] = ([], [], [])
unzipTriples ( (a,b,c) : ts ) = let (l1, l2, l3) = unzipTriples ts
                      in ( a:l1, b:l2, c:l3 )



merge :: (Eq a, Ord a) => [a] -> [a] -> [a]
merge [] [] = []
merge [] ys = ys
merge xs [] = xs
merge (x : xs) (y : ys) = if (x <= y)
                          then x : (merge xs (y : ys))
                          else y : (merge (x : xs) ys)


-- Takes 3 lists which are in sorted order and merges 
-- them so that the final list is sorted in increasing order.
mergeSorted3 :: Ord a => [a] -> [a] -> [a] -> [a]
mergeSorted3 [] [] [] = []
mergeSorted3 xs ys zs = merge xs (merge ys zs) 
