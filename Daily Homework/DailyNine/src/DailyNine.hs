-- Nicolas Abelanet
-- 10/28/21
-- Programming Languages
-- DailyNine.hs

module DailyNine where
    
-- minAndMax finds the min and max of a list of numbers in
-- in 3[n/2] comparisons. Consumes a number and list a numbers
-- and returns a tuple of the form (min, max)
minAndMax :: (Num a, Ord a) => [a] -> (a, a)
minAndMax [] = error "Cannot find min and max of an empty list"
minAndMax (a : numbers) = minAndMaxRecursive (a,a) numbers

-- minAndMaxRecursive finds the min and max of a list of numbers
-- recursively. Consumes a tuple of the form (currentMin, currentMax) 
-- and list a numbers then returns a tuple of the form (min, max)
minAndMaxRecursive :: (Num a, Ord a) => (a, a) -> [a] -> (a,a)
minAndMaxRecursive (min, max) [] = (min, max)

minAndMaxRecursive (min, max) (x : y : numbers) 
                                        | (a < min) && (b > max) = minAndMaxRecursive (a, b) numbers
                                        | (b > max) = minAndMaxRecursive (min, b) numbers
                                        | (a < min) = minAndMaxRecursive (a, max) numbers

                                        | otherwise = minAndMaxRecursive (min, max) numbers

                                where (a,b) = if (x > y)
                                              then (y, x)
                                              else (x, y)
                                

minAndMaxRecursive (min, max) (a : numbers) | (a > max) = minAndMaxRecursive (min, a) numbers
                                            | (a < min) = minAndMaxRecursive (a, max) numbers

                                            | otherwise = minAndMaxRecursive (min, max) numbers


-- everyK consumes a number and a list.
-- The function produces every kth element of the list.
everyK :: Int -> [a] -> [a]
everyK k list = everyKRecursive k k list

-- everyKRecursive consumes 2 numbers and a list.
-- The first number is the k and the second the current element.
-- The function should producs every kth element of the list.
everyKRecursive :: Int -> Int -> [a] -> [a]
everyKRecursive k n [] = []
everyKRecursive k 1 (x : xs) = x : (everyKRecursive k k xs)
everyKRecursive k n (x : xs) = everyKRecursive k (n-1) xs 

-- shuffle consumes two lists of the same type elements and 
-- produces one list which contains alternating elements from 
-- the given lists.
shuffle :: [a] -> [a] -> [a]
shuffle [] [] = []
shuffle [] list2 = shuffle list2 []
shuffle (a : list1) [] = a : (shuffle list1 []) 
shuffle (a : list1) (b : list2) = a : b : (shuffle list1 list2) 

    