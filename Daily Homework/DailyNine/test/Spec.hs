-- Nicolas Abelanet
-- 10/25/21
-- Programming Languages
-- Spec.hs

module Main where

import Test.Hspec
import DailyNine

main :: IO ()
main = hspec $ do
  describe "minAndMax" $ do
    it "found the min and max of [1,2,3,4,5]" $
      (minAndMax [1,2,3,4,5]) `shouldBe` (1,5)

    it "found the min and max of [1]" $
      (minAndMax [1]) `shouldBe` (1,1)

    it "found the min and max of [-5,-2,1,5,2,-4,10,-4]" $
      (minAndMax [-5,-2,1,5,2,-4,10,-4]) `shouldBe` (-5,10)

    it "found the min and max of [-1,1,1,1,-1,0,1,1,1,1,1,1,1]" $
      (minAndMax [-1,1,1,1,-1,0,1,1,1,1,1,1,1]) `shouldBe` (-1,1)

  describe "everyK" $ do
    it "found every element of [1,2,3,4,5]" $
      (everyK 1 [1,2,3,4,5]) `shouldBe` [1,2,3,4,5]

    it "found every 2nd element of [1,2,3,4,5]" $
      (everyK 2 [1,2,3,4,5]) `shouldBe` [2,4]

    it "found every 5th element of [1,2,3,4,5]" $
      (everyK 5 [1,2,3,4,5]) `shouldBe` [5]

    it "found every 10th element of [1,2,3,4,5]" $
      (everyK 10 [1,2,3,4,5]) `shouldBe` []

  describe "shuffle" $ do
    it "shuffled [1,3,5] and [2,4,6]" $
      (shuffle [1,3,5] [2,4,6]) `shouldBe` [1,2,3,4,5,6]

    it "shuffled [1,3,5] and []" $
      (shuffle [1,3,5] []) `shouldBe` [1,3,5]

    it "shuffled [] and [2,4,6]" $
      (shuffle [] [2,4,6]) `shouldBe` [2,4,6]

    it "shuffled [1] and [2,4,6]" $
      (shuffle [1] [2,4,6]) `shouldBe` [1,2,4,6]