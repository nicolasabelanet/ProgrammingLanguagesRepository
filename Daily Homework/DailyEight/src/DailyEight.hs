module DailyEight where

-- Event has these fields: name, day, 
-- month, year, xlocation, ylocation. 
-- This structure represents events which 
-- occurred on the specified date and at the 
-- given location.
data Event = Event 
    { name :: String
    , day :: Int
    , month :: String
    , year :: Int
    , xlocation :: Double
    , ylocation :: Double }
    deriving(Show)

-- inYear takes a number called targetYear, and a list of event structures 
-- and produces a new list. Each element in the new list should 
-- be an event structure which occurred during year.
inYear :: Int -> [Event] -> [Event]
inYear targetYear events = filter (\event -> (year event) == targetYear) events

-- inDayRange takes two days (a start day and an end day) and a list of event 
-- structures and produces a new list. Each element in the new list should 
-- be the name of all events which occurred between the two days (including 
-- start day and end day) but in any month or year.
inDayRange :: Int -> Int -> [Event] -> [String]
inDayRange startDay endDay events = map (name) (filter (\event -> ((day event) >= startDay) && ((day event) <= endDay)) events)

-- inArea takes a name, a lower x location, an lower y location, a upper x location, 
-- an upper y location and a list of event structures and produces a new list. 
-- Each element in the new list should be the event structures which match the name 
-- and occurred in the spatial region specified.
inArea :: String -> Double -> Double -> Double -> Double -> [Event] -> [Event]
inArea targetName lowerX lowerY upperX upperY events = filter (\event -> 
    (inAreaCriteria targetName event lowerX lowerY upperX upperY)) events

-- inAreaCritera takes a targetName, an Event, a lower x location, an lower y location, 
-- a upper x location, an upper y location and produces a bool. This bool
-- will be true if the event matches the targetName and occurred in the spatial 
-- region specified and false otherwise.
inAreaCriteria :: String -> Event -> Double -> Double -> Double -> Double -> Bool
inAreaCriteria targetName event lowerX lowerY upperX upperY = ((xlocation event) >= lowerX) &&
                                           ((xlocation event) <= upperX) &&
                                           ((ylocation event) >= lowerY) &&
                                           ((ylocation event) <= upperY) &&
                                           ((name event) == targetName)

-- identicalList takes in 2 lists of events and produces a bool.
-- This bool will be true if every element in list 1 is in list 2 and vice versa
-- and false otherwise.
identicalList :: [Event] -> [Event] -> Bool
identicalList [] [] = True
identicalList _ [] = False
identicalList [] _ = False
identicalList events1 events2 = (subset events1 events2) && (subset events2 events1)

-- subset takes in 2 lists of events and produces a bool.
-- This bool is true if an event is present in the first list
-- then it is present in the second and false otherwise.
subset :: [Event] -> [Event] -> Bool
subset [] events2 = True
subset (e1 : events1) events2 = (contains e1 events2) && (subset events1 events2)
-- contains takes in an event and a list of events and produces a bool.
-- This bool is true if the event is present in the list of events and
-- false otherwise.
contains :: Event -> [Event] -> Bool
contains e1 [] = False
contains e1 (e2 : events2)= (identical e1 e2) || (contains e1 events2) 

-- identicalList takes in 2 events and produces a bool.
-- This bool will be true if every element in list 1 is identical to every element
-- in list 2 and false otherwise.
identical :: Event -> Event -> Bool
identical event1 event2 = ((name event1) == (name event2)) &&
                          ((day event1) == (day event2)) &&
                          ((month event1) == (month event2)) &&
                          ((year event1) == (year event2)) &&
                          ((xlocation event1) == (xlocation event2)) &&
                          ((ylocation event1) == (ylocation event2))