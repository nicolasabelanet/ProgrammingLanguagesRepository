-- Nicolas Abelanet
-- 10/25/21
-- Programming Languages
-- Spec.hs

module Main where

import Test.Hspec
import DailyEight

main :: IO ()
main = hspec $ do
  describe "inYear" $ do
    it "found all the events that happened in 2021" $
      (identicalList (inYear 2021 [Event {name = "Birthday Party", day = 30, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}]) [Event {name = "Birthday Party", day = 30, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}] ) `shouldBe` True

    it "found all the events that happened in 2020" $
      (identicalList (inYear 2020 [Event {name = "Birthday Party", day = 30, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}]) [] ) `shouldBe` True

    it "found all the events that happened in 2021" $
      (identicalList (inYear 2021 [Event {name = "Birthday Party", day = 30, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}, Event {name = "Birthday Party", day = 30, month = "Mar", year = 2020, xlocation = 1.5, ylocation = 2.3}]) [Event {name = "Birthday Party", day = 30, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}] ) `shouldBe` True
     
  describe "inDayRange" $ do
    it "found all the events between the 1st and 31st" $
      (inDayRange 1 31 [Event {name = "Event1", day = 2, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}, Event {name = "Event2", day = 5, month = "Mar", year = 2020, xlocation = 1.5, ylocation = 2.3}]) `shouldBe` ["Event1", "Event2"]
    
    it "found all the events between the 1st and 3rd" $
      (inDayRange 1 3 [Event {name = "Event1", day = 2, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}, Event {name = "Event2", day = 5, month = "Mar", year = 2020, xlocation = 1.5, ylocation = 2.3}]) `shouldBe` ["Event1"]

    it "found all the events between the 1st and 2nd" $
      (inDayRange 1 3 [Event {name = "Event1", day = 4, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}, Event {name = "Event2", day = 5, month = "Mar", year = 2020, xlocation = 1.5, ylocation = 2.3}]) `shouldBe` []

  describe "inArea" $ do
    it "found all the events in the bounds 0 0 to 1 1 with the name Event2" $
      (identicalList (inArea "Event2" 0 0 1 1 [Event {name = "Event1", day = 2, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}, Event {name = "Event2", day = 5, month = "Mar", year = 2020, xlocation = 1.5, ylocation = 2.3}]) [])`shouldBe` True

    it "found all the events in the bounds 0 0 to 4 4 with the name Event2" $
      (identicalList (inArea "Event2" 0 0 4 4 [Event {name = "Event1", day = 2, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}, Event {name = "Event2", day = 5, month = "Mar", year = 2020, xlocation = 1.5, ylocation = 2.3}]) [Event {name = "Event2", day = 5, month = "Mar", year = 2020, xlocation = 1.5, ylocation = 2.3}] )`shouldBe` True

    it "found all the events in the bounds 0 0 to 4 4 with the name Event1" $
      (identicalList (inArea "Event1" 0 0 4 4 [Event {name = "Event1", day = 2, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}, Event {name = "Event2", day = 5, month = "Mar", year = 2020, xlocation = 1.5, ylocation = 2.3}]) [Event {name = "Event1", day = 2, month = "Mar", year = 2021, xlocation = 1.5, ylocation = 2.3}] )`shouldBe` True

    

    