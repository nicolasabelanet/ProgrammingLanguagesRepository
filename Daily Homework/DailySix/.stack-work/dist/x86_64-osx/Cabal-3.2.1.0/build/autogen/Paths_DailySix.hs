{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_DailySix (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailySix/.stack-work/install/x86_64-osx/e9ae070c5a47a1e5412d7c628e4e28f8b9cdb04fcd4ab41126b6a6b43bf106c1/8.10.7/bin"
libdir     = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailySix/.stack-work/install/x86_64-osx/e9ae070c5a47a1e5412d7c628e4e28f8b9cdb04fcd4ab41126b6a6b43bf106c1/8.10.7/lib/x86_64-osx-ghc-8.10.7/DailySix-0.1.0.0-AN2T9rci9Gd77hnLd8GbKF"
dynlibdir  = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailySix/.stack-work/install/x86_64-osx/e9ae070c5a47a1e5412d7c628e4e28f8b9cdb04fcd4ab41126b6a6b43bf106c1/8.10.7/lib/x86_64-osx-ghc-8.10.7"
datadir    = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailySix/.stack-work/install/x86_64-osx/e9ae070c5a47a1e5412d7c628e4e28f8b9cdb04fcd4ab41126b6a6b43bf106c1/8.10.7/share/x86_64-osx-ghc-8.10.7/DailySix-0.1.0.0"
libexecdir = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailySix/.stack-work/install/x86_64-osx/e9ae070c5a47a1e5412d7c628e4e28f8b9cdb04fcd4ab41126b6a6b43bf106c1/8.10.7/libexec/x86_64-osx-ghc-8.10.7/DailySix-0.1.0.0"
sysconfdir = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailySix/.stack-work/install/x86_64-osx/e9ae070c5a47a1e5412d7c628e4e28f8b9cdb04fcd4ab41126b6a6b43bf106c1/8.10.7/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "DailySix_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "DailySix_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "DailySix_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "DailySix_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "DailySix_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "DailySix_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
