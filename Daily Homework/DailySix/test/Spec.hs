module Main where

import Test.Hspec
import DailySix

main :: IO ()
main = hspec $ do
  describe "shorterThan" $ do
    it "removed all words shorter than or equal to 5" $
      (shorterThan 3 ["One", "Two", "Three", "Four"]) `shouldBe` ["One", "Two"]

    it "removed all words shorter than or equal to 2" $
      (shorterThan 2 []) `shouldBe` []

    it "removed all words shorter than or equal to 0" $
      (shorterThan 0 ["None", "of", "these", "should", "be", "present"]) `shouldBe` []

  describe "removeMultiples" $ do
    it "removed all multiples of 1" $
      (removeMultiples 1 [1,2,3,4,5,6,7,8,9,10]) `shouldBe` []

    it "removed all multiples of 5" $
      (removeMultiples 5 [1,2,3,4,5,6,7,8,9,10]) `shouldBe` [1,2,3,4,6,7,8,9]

    it "removed all multiples of 2" $
      (removeMultiples 2 [1,2,3,4,5,6,7,8,9,10]) `shouldBe` [1,3,5,7,9]

  describe "onlyJust" $ do
    it "created a list with only justs" $
      (onlyJust ([Nothing, Nothing, Nothing, Nothing] :: [Maybe Int]) ) `shouldBe` ([] :: [Maybe Int])

    it "created a list with only justs" $
      (onlyJust [Just 1, Nothing, Just 2, Nothing]) `shouldBe` [Just 1, Just 2]

    it "created a list with only justs" $
      (onlyJust [Just 1, Just 2, Just 3, Just 4]) `shouldBe` [Just 1, Just 2, Just 3, Just 4]

