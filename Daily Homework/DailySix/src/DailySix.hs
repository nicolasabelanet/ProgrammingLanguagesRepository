module DailySix where

-- shorterThan consumes a number and a list of words. 
-- The function should produce a list of the words whose 
-- length is shorter than or equal to the given number.
shorterThan :: Int -> [[Char]] -> [[Char]]
shorterThan minLength words = filter (\word -> (length word) <= minLength) words

-- removeMultiples consumes a number and a list of numbers. 
-- The function should produce a list where the 
-- multiples of the given number have been removed.
removeMultiples ::(Num a, Integral a) => a -> [a] -> [a]
removeMultiples target nums = filter (\num -> (mod num target) /= 0 ) nums

-- onlyJust consumes a list of Maybe a and produces 
-- a list where all values of Nothing have been eliminated
onlyJust :: (Eq a) => [Maybe a] -> [Maybe a]
onlyJust maybes = filter (\x -> x /= Nothing) maybes