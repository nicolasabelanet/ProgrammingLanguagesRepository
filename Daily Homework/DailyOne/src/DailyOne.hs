-- Nicolas Abelanet
-- 9/19/21
-- Programming Languages
-- DailyOne.hs

module DailyOne where
 
-- quadratic takes four generic numbers: a, b, c, and x. 
-- The function returns the value of a + b*x + c*x^2 as a generic number.
quadratic :: (Num a) => a -> a -> a -> a -> a
quadratic a b c x = a + (b * x) + (c * (x^2))

-- scaleVector takes a single scalar as a generic number along with a 2-tuple of generic 
-- numbers which represents a two dimensional vector. 
-- The function returns a 2-tuple of generic numbers which represents the vector scaled by the value. 
-- For example, scaleVector of 5 and (3, 4) should produce (15, 20).

-- I tried several times trying with just the generic defintion of
-- x -> (a,b) -> (a,b) and the program did not work. I am not sure why.

scaleVector :: (Num a) => a -> (a, a) -> (a, a)
scaleVector x (a, b) = (a * x, b * x)

-- tripleDistance takes two 3-tuples of floats which represent three dimensional 
-- points and finds the cartesian distance between them returned as a float. 
tripleDistance :: (Floating a) => (a, a, a) -> (a, a, a) -> a
tripleDistance (x1, y1, z1) (x2, y2, z2) = sqrt( ( (x2 - x1)^2 ) + ( (y2 - y1)^2 ) + ( (z2 - z1)^2) )


 