{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_DailyOne (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailyOne/.stack-work/install/x86_64-osx/458f4a382f79c1fab2af41cce0e662c44e48e487dbc7f13ed004aa5e78388217/8.10.7/bin"
libdir     = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailyOne/.stack-work/install/x86_64-osx/458f4a382f79c1fab2af41cce0e662c44e48e487dbc7f13ed004aa5e78388217/8.10.7/lib/x86_64-osx-ghc-8.10.7/DailyOne-0.1.0.0-8C4KWMnToMl7AqOvwn7ZX6"
dynlibdir  = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailyOne/.stack-work/install/x86_64-osx/458f4a382f79c1fab2af41cce0e662c44e48e487dbc7f13ed004aa5e78388217/8.10.7/lib/x86_64-osx-ghc-8.10.7"
datadir    = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailyOne/.stack-work/install/x86_64-osx/458f4a382f79c1fab2af41cce0e662c44e48e487dbc7f13ed004aa5e78388217/8.10.7/share/x86_64-osx-ghc-8.10.7/DailyOne-0.1.0.0"
libexecdir = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailyOne/.stack-work/install/x86_64-osx/458f4a382f79c1fab2af41cce0e662c44e48e487dbc7f13ed004aa5e78388217/8.10.7/libexec/x86_64-osx-ghc-8.10.7/DailyOne-0.1.0.0"
sysconfdir = "/Users/nicolasabelanet/Documents/Computer Science/Programming Languages/ProgrammingLanguagesRepository/Daily Homework/DailyOne/.stack-work/install/x86_64-osx/458f4a382f79c1fab2af41cce0e662c44e48e487dbc7f13ed004aa5e78388217/8.10.7/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "DailyOne_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "DailyOne_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "DailyOne_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "DailyOne_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "DailyOne_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "DailyOne_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
