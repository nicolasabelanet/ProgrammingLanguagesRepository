-- Nicolas Abelanet
-- 9/19/21
-- Programming Languages
-- Spec.hs

module Main where

import Test.Hspec
import DailyOne

main :: IO ()
main = hspec $ do
  describe "quadratic" $ do
    it "produces the quadratic of 1, 2, 3, and 4" $
      (quadratic 1 2 3 4) `shouldBe` 57

    it "produces the quadratic of 5, -6, 7, and 8" $
      (quadratic 5 (-6) 7 8) `shouldBe` 405

    it "produces the quadratic of 2.1, -4.56, 7.86, and 9" $
      (quadratic 2.1 (-4.56) 7.86 9) `shouldBe` 597.72

    it "produces the quadratic of 150, 151, 152, and 153" $
      (quadratic 150 151 152 153) `shouldBe` 3581421

  describe "scaleVector" $ do
    it "produces a scaled vector of (-1,2) by a factor of 5" $
      (scaleVector 5 ((-1),2)) `shouldBe` ((-5), 10)

    it "produces a scaled vector of (1,2) by a factor of -10" $
     (scaleVector (-10) (1.25, 2.5)) `shouldBe` ((-12.5), (-25))

    it "produces a scaled vector of (2,3) by a factor of 1.5" $
      (scaleVector 1.5 (2, 3)) `shouldBe` (3, 4.5)

    it "produces a scaled vector of (1, 2) by a factor of 125" $
      (scaleVector 125 (1, 2)) `shouldBe` (125, 250)

  describe "tripleDistance" $ do
    it "produces the distance between  (54.301, 61.201, 157.777) and (162.694, 80.126, 62.829)" $
      (tripleDistance (54.301, 61.201, 157.777) (162.694, 80.126, 62.829)) `shouldBe` 145.3352014413576

    it "produces the distance between (195.193, 101.689, 129.216) and (106.338, 76.322, 173.784)" $
      (tripleDistance (195.193, 101.689, 129.216) (106.338, 76.322, 173.784)) `shouldBe` 102.59143403813012

    it "produces the distance between (-15.696, 70.466, -117.204) and (133.622, -95.894, 170.418)" $
      (tripleDistance (-15.696, 70.466, -117.204) (133.622, -95.894, 170.418)) `shouldBe` 364.2772702324426

    it "produces the distance between (196.05, 40.385, 41.665) and (76.893, 163.007, 110.851)" $
      (tripleDistance (196.05, 40.385, 41.665) (76.893, 163.007, 110.851)) `shouldBe` 184.4484972261905